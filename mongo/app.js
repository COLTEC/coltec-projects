var mongoose = require("mongoose");
mongoose.connect('mongodb://localhost/enduro-db');

var db = mongoose.connection;


db.on('error', console.error)
db.on('open', function(){
    console.log("Conectado ao MongoDB")

    var participanteSchema = new mongoose.Schema({
        nome: String,
        matricula: String
    })

    var Participante = mongoose.model('Participante', participanteSchema);
    
    var participante1 = new Participante({ nome: 'Bryan', matricula: '2016952932' });

    console.log(participante1)

})