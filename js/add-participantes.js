
// Script para adicionar os participantes por meio de um botão

$("button").click(function(event) {

    var qtd_participantes = document.getElementsByClassName("dados-participante").length;
    var add = document.getElementById("add-button")

    // IE (edge)
    event.cancelBubble = true;
    event.returnValue = false;
    
    // Firefox
    if (event.stopPropagation) {
        event.stopPropagation();
        event.preventDefault();
    }

    // Declarando os elementos que serão usados
    var div_12_colunas    = document.createElement("div");
    var field_nome        = document.createElement("div");
    var field_matricula   = document.createElement("div");
    var input_nome        = document.createElement("input");
    var input_matricula   = document.createElement("input");
    var divider           = document.createElement("hr");


    // Maximo de participantes (não incluindo o líder)
    var max_participantes = 6;

    // Conteudo
    div_12_colunas.className  = "dados-participante";

    // Fields de nome e matricula
    field_nome.className      = "row input_field col l6 m6 s12";
    field_matricula.className = "row input_field col L6 m6 s12";

    // Divider
    divider.className = "row divisor divider hide-on-med-and-up col s4 offset-s4";

    // Inputs
    input_nome.type = "text";
    input_nome.placeholder = "Nome Completo"

    input_matricula.type = "text";
    input_matricula.placeholder = "Matrícula da UFMG";


    if (qtd_participantes <= max_participantes) {

        input_nome.className = "p-nome";
        input_matricula.className = "p-matricula";

        field_nome.append(input_nome);
        field_matricula.append(input_matricula);
        div_12_colunas.append(divider,field_nome,field_matricula);

        document.getElementById("conteudo").appendChild(div_12_colunas);

        console.log(qtd_participantes)

        if (qtd_participantes == max_participantes) {
            $(".btn.blue").prop("disabled", true);
            $(".add").text("Equipe cheia!")
        }
    }
});


/* *********** Teste dos dados obtidos *********** */

var submit = document.getElementById("btn-prox");


// Printa no log o nome e a matrícula de cada usuário
submit.addEventListener("click", ()=>{

    var nomes = document.getElementsByClassName("p-nome")
    var matriculas = document.getElementsByClassName("p-matricula")

    var dados = []

    // Substituir isso depois para o envio ao banco de dados
    for (let i = 0; i < nomes.length; i++) {
        
        dados.push(
            {
                "nome" : nomes[i].value,
                "matricula" : matriculas[i].value
            }
        )
        
    }
    console.log(dados)

})

